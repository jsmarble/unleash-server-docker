# Summary
This repo contains a docker-compose file to start an Unleash server with a PostreSQL database.

# Persistence
The database can optionally be persistent by mapping a volume or folder to `/var/lib/postgresql/data`.

